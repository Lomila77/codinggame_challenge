import sys
import math
from enum import Enum, auto
from collections import namedtuple

Point = namedtuple("Point", ["x", "y"])
#p = Point(0, 2)

def debug(*msg):
    print(*msg, file=sys.stderr, flush = True)

import inspect

def deb(hero):
    if hero.hero_id == 0:
        caller_frame = sys._getframe(1)
        caller_frameinfo = inspect.getframeinfo(caller_frame)
        debug("line", caller_frameinfo.lineno)

class Mod(Enum):
    STANDARD = auto()
    DEFENCIF = auto()
    OFFENSIF = auto()

def is_in_wind(wind_casted, target):
    for wind in wind_casted:
        if target.distance_to(wind.x, wind.y) <= Hero.wind_radius:
            return True
    return False

class Entity:
    def __init__(self):
        (
            self.id,
            self.type,
            x,
            y,
            self.shield_life,
            self.is_controlled,
            self.health,
            vx,
            vy,
            self.near_base,
            self.threat_for
        ) = [int(j) for j in input().split()]
        self.position = Point(x, y)
        self.direction = Point(vx, vy)
        self.hero_id = self.id % 3

    def __lt__(self, other):
        return self.distance_to_base() < other.distance_to_base()

    def distance_to(self, x, y):
        return math.sqrt((self.position.x - x) ** 2 + (self.position.y - y) ** 2)

    def distance_to_base(self):
        return self.distance_to(base.position.x, base.position.y)

    def distance_from(self, other):
        return other.distance_to(self.position.x, self.position.y)

    def in_perim(self, point, radius):
        if self.distance_to(point.x, point.y) > radius:
            return False
        else:
            return True

    def out_perim(self, point, radius):
        if self.distance_to(point.x, point.y) > radius:
            return True
        else:
            return False

    def in_perim_of_hero(self, hero, base):
        radius = 4400
        if hero.hero_id == 0:
            area = post.base
        elif hero.hero_id == 1:
            if base.mod is Mod.DEFENCIF:
                area = post.base_front
            elif base.mod is Mod.OFFENSIF:
                area = post.north
                if base.pos_hero_1 == 1:
                    area = post.south
            else:
                area = post.west
                if base.pos_hero_1 == 1:
                    area = post.north
        else:
            if base.mod is Mod.DEFENCIF:
                area = post.mid
            elif base.mod is Mod.OFFENSIF:
                # area = post.base_ennemy
                # if base.pos_hero_2 == 1:
                area = post.base_ennemy_front
            else:
                area = post.south
                if base.pos_hero_2 == 1:
                    area = post.east
        if self.distance_to(area.x, area.y) > radius:
               return False
        else:
               return True



class Hero(Entity):
    def which_ennemy_next_to_hero(self, targets):
        radius = 2200
        for target in targets:
            if target.in_perim(self.position, radius):
                return target
        return None

    def which_hero_ennemy(self, heroes):
        for ennemy in heroes:
            if ennemy.distance_to_base() < 5000:
                return ennemy
        return None

    def ennemy_in_area(self, targets):
        ennemy_find = 0
        for target in targets:
            if target.in_perim_of_hero(self, base):
                ennemy_find += 1
        return ennemy_find

    def ennemy_next_to_hero_large(self, targets):
        ennemy_find = 0
        radius = 2200
        for target in targets:
            if target.in_perim(self.position, radius):
                ennemy_find += 1
        return ennemy_find

    def ennemy_next_to_hero(self, targets):
        ennemy_find = 0
        radius = 1280
        for target in targets:
            if target.in_perim(self.position, radius):
                ennemy_find += 1
        return ennemy_find

    def ennemy_shield_in_area(self, targets):
        ennemy_find = 0
        for target in targets:
            if target.in_perim_of_hero(self, base) and target.shield_life != 0:
                ennemy_find += 1
        return ennemy_find

    def fighting_radius(self):
        return 6000

    def hero_in_perim(self, base):
        return self.in_perim_of_hero(self.hero_id, base)

    def wait(self):
        print("WAIT")

    melee_radius = 800
    def move(self, point):
        print("MOVE %d %d HERO%d" % (point.x, point.y, self.hero_id))

    def attack(self, target):
        invert = 1 if player_id == 0 else -1
        position_attack = Point(target.position.x + target.direction.x - 200 * invert, target.position.y + target.direction.y - 200 * invert)
        if (
                target.threat_for != 2
                and ((hero.hero_id != 0 and target.distance_to_base() > 5000
                     and base.mod is not Mod.DEFENCIF)
                     or hero.hero_id == 0)
            ):
            self.move(position_attack)
            return True
        else:
            return False

    wind_radius = 1280
    def wind(self, point):
        print("SPELL WIND %d %d" %(point.x, point.y))

    def control(self, target):
        print("SPELL CONTROL %d %d %d" %(target.id, base.position_ennemy.x, base.position_ennemy.y))

    def control_classic(self, target, position):
        print("SPELL CONTROL %d %d %d" %(target.id, position.x, position.y))

    def control_base(self, target):
        print("SPELL CONTROL %d %d %d" %(target.id, post.mid.x, post.mid.y))

    def shield(self, target):
        print("SPELL SHIELD %d" %(target.id))

    def wind_def(self, target):
        if (
                target.distance_to_base() < 1280
                and stats[0].mana > 10
                and target.shield_life == 0
                and self.in_perim(target.position, self.wind_radius)
            ):
            self.wind(base.position_ennemy)
            return True
        return False

    def wind_att(self, target):
        if (
                target.shield_life == 0
                and stats[0].mana >= 60
                and target.distance_from(self) <= 1280

        ):
            self.wind(base.position_ennemy)
            return True
        return False

    def be_my_friend(self, target):
        invert = 0 if player_id == 0 else -1
        if (
                self.hero_id != 0
                and self.position.x > 6000 * invert
                and target.is_controlled == 0
                and target.threat_for != 2
                and target.health >= 18
                and stats[0].mana > 30
                and target.distance_to_base() > 5000
                and target.distance_from(self) <= 2200
        ):
            self.control(target)
            return True
        return False

    def assault(self, target):
        if (
                target.is_controlled == 0
                and stats[0].mana > 30
                and target.threat_for != 2
                and target.distance_from(self) <= 2200
        ):
            self.control(target)
            return True
        return False

    def in_coin(self, heroes_ennemy):
        find = -1
        for idx in range(len(heroes_ennemy)):
            if heroes_ennemy[idx].distance_from(self) < heroes_ennemy[find].distance_from(self):
                find = idx
        if (
                find != -1
                and heroes_ennemy[find].shield_life == 0
                and stats[0].mana >= 20
                and heroes_ennemy[find].distance_from(self) <= 2200
        ):
            self.control(heroes_ennemy[find])
            return True
        return False

    def out_coin(self, heroes_ennemy):
        find = -1
        for idx in range(len(heroes_ennemy)):
            if heroes_ennemy[idx].distance_from(self) < heroes_ennemy[find].distance_from(self):
                find = idx
        if (
                find != -1
                and heroes_ennemy[find].shield_life == 0
                and stats[0].mana >= 20
                and heroes_ennemy[find].distance_from(self) <= 2200
        ):
            self.control_base(heroes_ennemy[find])
            return True
        return False

    def get_over_here(self, target):
        if (
                target.shield_life == 0
                and stats[0].mana >= 20
                and target.distance_from(self) <= 1280
        ):
            self.control_base(target)
            #self.control_classic(target, target.position)
            #self.wind_att(target)
            return True
        return False

    def shield_def(self, heroes_ennemy):
        if (
                self.ennemy_next_to_hero_large(heroes_ennemy) > 0
                and stats[0].mana >= 10
                and stats[1].mana >= 10
                and self.shield_life == 0
        ):
            self.shield(self)
            return True
        return False

    def be_tortank(self, target):
        invert = 0 if player_id == 0 else -1
        if (
                self.hero_id != 0
                and target.threat_for == 2
                and target.health >= 17
                and stats[0].mana >= 30
                and target.distance_to(base.position_ennemy.x, base.position_ennemy.y) < 6000
                and target.shield_life == 0
                and target.distance_from(self) <= 2200
        ):
            self.shield(target)
            return True
        return False

    def guard(self, base):
        if self.hero_id == 0:
            self.move(post.base)
        elif self.hero_id == 1:
            if base.mod is Mod.STANDARD:
                if self.position == post.west:
                    base.pos_hero_1 = 1
                elif self.position == post.north:
                    base.pos_hero_1 = 0
                if base.pos_hero_1 == 1:
                    self.move(post.north)
                else:
                    self.move(post.west)
            elif base.mod is Mod.OFFENSIF:
                if self.position == post.west:
                    base.pos_hero_1 = 1
                elif self.position == post.north:
                    base.pos_hero_1 = 0
                if base.pos_hero_1 == 1:
                    self.move(post.north)
                else:
                    self.move(post.west)
            else:
                self.move(post.base_front)
        else:
            if base.mod is Mod.STANDARD:
                if self.position == post.south:
                    base.pos_hero_2 = 1
                elif self.position == post.east:
                    base.pos_hero_2 = 0
                if base.pos_hero_2 == 1:
                    self.move(post.east)
                else:
                    self.move(post.south)
            elif base.mod is Mod.DEFENCIF:
                self.move(post.mid)
            else:
                # if self.position == post.base_ennemy:
                #     base.pos_hero_2 = 1
                # elif self.position == post.base_ennemy_front:
                #     base.pos_hero_2 = 0
                # if base.pos_hero_2 == 1:
                #     self.move(post.base_ennemy_front)
                # else:
                self.move(post.base_ennemy_front)

    def guard_offenciv(self):
        base.mod = Mod.OFFENSIF

    def guard_defenciv(self):
        base.mod = Mod.DEFENCIF


class Stats:
    def __init__(self):
        self.health, self.mana = [int(j) for j in input().split()]


class Base:
    def __init__(self):
        x, y = [int(i) for i in input().split()]
        self.position = Point(x , y)
        player_id = 0 if self.position.x == 0 else 1
        ennemie_x = 0 if self.position.x != 0 else 17630
        ennemie_y = 0 if self.position.x != 0 else 9000
        self.position_ennemy = Point(ennemie_x, ennemie_y)
        self.pos_hero_1 = 0
        self.pos_hero_2 = 0
        self.mod = Mod.STANDARD
        self.ennemy_in = False

class Post:
    def __init__(self):
        invert = 1 if base.position.x == 0 else -1
        self.base = Point(base.position.x + 1500 * invert, base.position.y + 1500 * invert)
        self.base_front = Point(base.position.x + 4000 * invert, base.position.y + 4000 * invert)
        self.base_ennemy = Point(base.position_ennemy.x - 1500 * invert, base.position_ennemy.y - 1500 * invert)
        self.base_ennemy_front = Point(base.position_ennemy.x - 5000 * invert, base.position_ennemy.y - 3500 * invert)
        self.mid = Point(base.position.x + 8757 * invert, base.position.y + 4580 * invert)
        self.north = Point(base.position.x + 9000 * invert, base.position.y + 2000 * invert)
        self.south = Point(base.position.x + 9000 * invert, base.position.y + 7000 * invert)
        self.west = Point(base.position.x + 5500 * invert, base.position.y + 5500 * invert)
        self.east = Point(base.position.x + 12500 * invert, base.position.y + 3000 * invert)
        self.frontiere = Point(base.position.x + 5000 * invert, base.position.y + 3500 * invert)
        self.north_frontiere = Point(base.position.x + 2668 * invert, base.position.y + 5629 * invert)
        self.south_frontiere = Point(base.position.x + 7054 * invert, base.position.y + 2465 * invert)

class Controlled:
    def __init__(self):
        self.heroes_ennemy  = False
        self.position_ennemy = Point(0, 0)

base = Base()
post = Post()
player_id = 0 if base.position.x == 0 else 1
heroes_per_player = int(input())  # Always 3
tour = 0
controled = Controlled()


# Lancement du jeu
while True:
    stats = [Stats(), Stats()]
    targets = []
    heroes = []
    wind_casted = []
    heroes_ennemy = []
    tour += 1

    debug(base.mod)

    #Comptage et classement des unites
    #Si c'est un ennemie je l'ajoute a la liste des cibles (target[])
    #Si c'est un Hero je l'ajoute a la liste des heros (heroes[])
    entity_count = int(input())
    for i in range(entity_count):
        entity = Entity()
        if entity.type == 1:
            entity.__class__ = Hero
            heroes.append(entity)
        if entity.type == 0: # and entity.threat_for == 1:
            targets.append(entity)
        elif entity.type == 2:
            heroes_ennemy.append(entity)

    #Trie des heros
    #heroes.sort()

    #Trie des cibles (voir Entity: __lt__)
    targets.sort()

    #Redefinition de hero en Hero (et non entity)
    #Si aucune cible n'a ete trouver
    #Alors il revient a son poste

    if (
            stats[0].mana < 10
            and heroes[0].ennemy_in_area(targets) >= 6
            and heroes[0].ennemy_shield_in_area(targets) > 1
            and base.mod is not Mod.DEFENCIF
     ):
         base.mod = Mod.DEFENCIF
    else:
        base.ennemy_in = False
        base.mod = Mod.STANDARD

    if (
            tour >= 100 and base.mod is not Mod.DEFENCIF
    ):
            base.mod = Mod.OFFENSIF

    #BOUCLE JEU HEROS
    for hero in heroes:
        if (
            hero.which_hero_ennemy(heroes_ennemy) is not None
        ):
            base.mod = Mod.DEFENCIF
            base.ennemy_in = True

        if len(targets) == 0:
            hero.guard(base)
            deb(hero)
    #Le hero 0 se shield so un ennemy est dans la base et declare l'alerte
        else:
            if hero.hero_id == 0:
                if (
                        hero.ennemy_next_to_hero_large(heroes_ennemy) > 0
                        and hero.ennemy_next_to_hero_large(targets) > 1
                ):
                    deb(hero)
                    if hero.shield_def(heroes_ennemy):
                        continue
            find_target = False
            best_idx = 0
            for idx in range(len(targets)):
                if targets[idx].in_perim_of_hero(hero, base):
                    deb(hero)
                    find_target = True
                    if hero.hero_id == 0 or (hero.hero_id == 1 and base.mod is Mod.DEFENCIF):
                        if targets[idx].distance_to_base() < targets[best_idx].distance_to_base():
                            best_idx = idx
                            deb(hero)
                    elif hero.hero_id == 2 and base.mod is Mod.OFFENSIF:
                        if targets[idx].health > targets[best_idx].health:
                            best_idx = idx
                    else:
                        if targets[idx].distance_from(hero) < targets[best_idx].distance_from(hero):
                            best_idx = idx
            if not find_target:
                deb(hero)
                hero.guard(base)
                continue
            elif (
                    not is_in_wind(wind_casted, targets[best_idx])
                    and hero.wind_def(targets[best_idx])
                ):
                wind_casted.append(hero.position)
                deb(hero)
            elif (
                    base.mod is Mod.DEFENCIF
                    and hero.hero_id == 1
            ):
                ennemy_find = hero.which_hero_ennemy(heroes_ennemy)
                if ennemy_find is not None:
                    if not hero.get_over_here(ennemy_find):
                        hero.guard(base)
                else:
                    hero.guard(base)
            elif base.mod is Mod.OFFENSIF and hero.hero_id != 0:
                if hero.ennemy_next_to_hero_large(heroes_ennemy) > 0:
                    if hero.hero_id == 2:
                        if hero.out_coin(heroes_ennemy):
                            continue
                    else:
                        if hero.assault(hero.which_ennemy_next_to_hero(heroes_ennemy)):
                            controled.position_ennemy = (hero.which_ennemy_next_to_hero(heroes_ennemy)).position
                            controled.heroes_ennemy = True
                            continue
                if hero.hero_id == 2:
                    if not hero.assault(targets[best_idx]):
                        if not hero.be_tortank(targets[best_idx]):
                            if not hero.attack(targets[best_idx]):
                                hero.guard(base)
                else:
                    if not hero.assault(targets[best_idx]):
                        if hero.attack(targets[best_idx]):
                            del targets[best_idx]
                        else:
                            if controled.heroes_ennemy is False:
                                hero.guard(base)
                            else:
                                hero.move(controled.position_ennemy);
                                controled.heroes_ennemy = False
            else:
                if not hero.be_my_friend(targets[best_idx]):
                    deb(hero)
                    if not hero.be_tortank(targets[best_idx]):
                        deb(hero)
                        if hero.attack(targets[best_idx]):
                            deb(hero)
                            del targets[best_idx]
                        else:
                            deb(hero)
                            hero.guard(base)
